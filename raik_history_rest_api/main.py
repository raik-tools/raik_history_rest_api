import asyncio
import pathlib

import uvicorn
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

import raik_history_rest_api.conf
from raik_history_rest_api.api import api_router
from raik_history_rest_api.views import router

STATIC_DIR = pathlib.Path(__file__).parent / "static"

app = FastAPI(debug=True)
app.mount("/static/", StaticFiles(directory=str(STATIC_DIR)), name="static")
app.include_router(api_router, prefix="/api")
app.include_router(router)


def serve(
    tasks: list = tuple(),
):
    config = raik_history_rest_api.conf.Config.get_config()

    uvi_config = uvicorn.Config(
        config.main_app,
        port=config.port,
        host=config.host,
        log_level=config.log_level.lower(),
        proxy_headers=config.proxy_headers,
        forwarded_allow_ips=config.forwarded_allow_ips,
    )
    server = uvicorn.Server(uvi_config)

    async def main():
        for t in tasks:
            asyncio.create_task(t)
        await server.serve()

    asyncio.run(main())
