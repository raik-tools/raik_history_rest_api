# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from typing import Annotated

from fastapi import APIRouter, Query

from raik_history.series import series
from raik_history.db import connection

from raik_history_rest_api.api.common import process_time_range
from raik_history_rest_api.api.schemas.calculations import CalculationResultList
from raik_history_rest_api.conf import Config

router = APIRouter()


@router.get(
    "/time_weighted_sum/{start_time_ms}/{end_time_ms}/",
    response_model=CalculationResultList,
)
async def get_time_weighted_sum(
    start_time_ms: int,
    end_time_ms: int,
    name: Annotated[list[str] | None, Query()],
    bad: bool = False,
    uncertain: bool = False,
):
    start_time_ms, end_time_ms = process_time_range(start_time_ms, end_time_ms)
    path = Config.get_config().db_path

    with connection.db_connection(path) as c:
        data = [
            {
                "series_name": s,
                "data": series.Series.from_db(name=s, connection=c).time_weighted_sum(
                    start_time=int(start_time_ms),
                    end_time=int(end_time_ms),
                    # rate_conversion=aggregates.TimeWeightedRateConversion.PerHour,
                    include_bad_values=bad,
                    include_uncertain_values=uncertain,
                    connection=c,
                ),
            }
            for s in name
        ]

    return {"count": len(data), "data": data}
