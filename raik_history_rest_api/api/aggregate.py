# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from typing import Annotated

from fastapi import APIRouter, Query

from raik_history.calculations import aggregates
from raik_history.db import connection

from raik_history_rest_api.api.common import process_time_range
from raik_history_rest_api.api.schemas.value import ValueList
from raik_history_rest_api.conf import Config

router = APIRouter()


@router.get("/{agg_type}/{start_time_ms}/{end_time_ms}/", response_model=ValueList)
async def get_simple_aggregate(
    agg_type: str,
    start_time_ms: int,
    end_time_ms: int,
    name: Annotated[list[str] | None, Query()],
    bad: bool = False,
    uncertain: bool = False,
):
    start_time_ms, end_time_ms = process_time_range(start_time_ms, end_time_ms)

    path = Config.get_config().db_path
    agg_fx = {
        "max": aggregates.get_max,
        "min": aggregates.get_min,
        "count": aggregates.get_count,
    }[agg_type]

    with connection.db_connection(path) as c:
        data = [
            {
                "series_name": s,
                "data": agg_fx(
                    c,
                    s,
                    int(start_time_ms),
                    int(end_time_ms),
                    bad,
                    uncertain,
                ),
            }
            for s in name
        ]

    return {"count": len(data), "data": data}
