# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from typing import Annotated, Optional

from fastapi import APIRouter, Query, Request, HTTPException, status

from raik_history.db import event_tables
from raik_history.db import connection
from raik_history.db.event_tables import EventInterface

from raik_history_rest_api.api.common import (
    process_time_range,
    get_method_return_type,
    get_method_return_type_string,
    get_method_return_absolute_error,
)
from raik_history_rest_api.api.schemas.event import (
    EventTypes,
    EventList,
    ProcessedEventList,
    EventDetails,
    ProcessedEvent,
    EventUpdate,
)
from raik_history_rest_api.conf import Config
from raik_history_rest_api.event_register import get_event_class

router = APIRouter()


@router.get("/types/", response_model=EventTypes)
async def get_event_types():
    path = Config.get_config().db_path

    with connection.db_connection(path) as c:
        data = event_tables.get_event_types(c)

    return {"count": len(data), "data": data}


@router.get("/types/{event_type_id}/", response_model=EventDetails)
async def get_event_type_details(event_type_id: int):
    path = Config.get_config().db_path

    with connection.db_connection(path) as c:
        event_data = event_tables.get_event_type_by(c, type_id=event_type_id)
        try:
            event_class = get_event_class(event_data["name"])
        except KeyError:
            event_class = None

    if event_class:
        processing_class = str(event_class)
        processed_columns = []
        for c_name in event_class.columns:
            c_type = get_method_return_type_string(event_class, c_name)
            a_error = get_method_return_absolute_error(event_class, c_name)
            processed_columns.append(
                {"name": c_name, "py_type": c_type, "absolute_error": a_error}
            )
    else:
        processing_class = None
        processed_columns = None

    return {
        "type": event_data,
        "processing_class": processing_class,
        "processed_columns": processed_columns,
    }


@router.get("/raw/{event_type_id}/", response_model=EventList)
async def get_multiple_events(
    event_type_id: int,
    start_ts__gte: Optional[int] = None,
    start_ts__lt: Optional[int] = None,
    comments__contains: Optional[str] = None,
    count: int = 500,
    chronological_order: bool = True,
    only_good_quality: bool = True,
):
    start_ts__gte, start_ts__lt = process_time_range(start_ts__gte, start_ts__lt)
    path = Config.get_config().db_path

    with connection.db_connection(path) as c:
        data = event_tables.get_multiple_events(
            c,
            event_type_id,
            start_ts__gte,
            start_ts__lt,
            comments__contains=comments__contains,
            count=count,
            chronological_order=chronological_order,
            only_good_quality=only_good_quality,
        )

    return {"count": len(data), "data": data}


@router.get("/raw/{event_type_id}/{start_time_ms}/", response_model=EventInterface)
async def get_raw_event(
    event_type_id: int,
    start_time_ms: int,
):
    path = Config.get_config().db_path

    with connection.db_connection(path) as c:
        e = event_tables.get_event(c, event_type_id, start_time_ms)

    return e


@router.post("/raw/", response_model=EventInterface)
async def post_raw_event(item: EventUpdate):
    path = Config.get_config().db_path
    with connection.db_connection(path) as c:
        event_tables.update_event(
            c,
            event_type_id=item.event_type_id,
            start_ts=item.start_ts,
            comments=item.comments,
        )
        e = event_tables.get_event(
            c, event_type_id=item.event_type_id, start_ts=item.start_ts
        )

    return e


_get_multiple_events_standard_args = {
    "start_ts__gte",
    "start_ts__lt",
    "comments__contains",
    "col",
    "count",
    "chronological_order",
    "only_good_quality",
}


@router.get(
    "/processed/{event_type_id}/",
    response_model=ProcessedEventList,
    openapi_extra={
        "parameters": [
            {
                "in": "query",
                "name": "f",
                "schema": {"type": "object", "additionalProperties": "true"},
                "style": "form",
                "explode": "true",
            }
        ]
    },
)
async def get_processed_events(
    event_type_id: int,
    request: Request,
    # Query Params:  Make sure that these are placed in _get_multiple_events_standard_args above
    start_ts__gte: Optional[int] = None,
    start_ts__lt: Optional[int] = None,
    comments__contains: Optional[str] = None,
    col: Annotated[list[str] | None, Query()] = None,
    count: int = 500,
    chronological_order: bool = True,
    only_good_quality: bool = True,
    # Query Params: See Above
):
    """
    Additional filters are provided for in the request object.  This only
    works if an Event Class exists (the filtering is performed after DB load
    """

    start_ts__gte, start_ts__lt = process_time_range(start_ts__gte, start_ts__lt)
    path = Config.get_config().db_path

    with connection.db_connection(path) as c:
        try:
            event_data = event_tables.get_event_type_by(c, type_id=event_type_id)
            event_name = event_data["name"]
            event_class = get_event_class(event_name)
        except KeyError:
            event_class = None

        if event_class:

            filter_args = {}
            for k in set(request.query_params) - _get_multiple_events_standard_args:
                v = request.query_params[k]
                try:
                    method_name, comparison = k.split("__")
                    return_type = get_method_return_type(event_class, method_name)
                    v = return_type(v)
                except (KeyError, ValueError):
                    pass
                filter_args[k] = v

            event_class.db_connection = c
            try:
                events = event_class.get_events(
                    start_ts__gte=start_ts__gte,
                    start_ts__lt=start_ts__lt,
                    comments__contains=comments__contains,
                    count=count,
                    chronological_order=chronological_order,
                    only_good_quality=only_good_quality,
                    **filter_args
                )
            except AttributeError as e:
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
                )
            data = []
            for e in events:
                if col:
                    processed_data = {
                        col_name: getattr(e, col_name)() for col_name in col
                    }
                else:
                    processed_data = e.columns_dict()
                data.append(
                    {
                        "event_type_id": event_type_id,
                        "name": event_name,
                        "start_ts": e.start_ts,
                        "end_ts": e.end_ts,
                        "comments": e.comments,
                        "quality": e.quality,
                        "columns": processed_data,
                    }
                )

        else:
            data = [
                {
                    "event_type_id": event_type_id,
                    "name": event_name,
                    "start_ts": x["start_ts"],
                    "end_ts": x["end_ts"],
                    "comments": x["comments"],
                    "quality": x["quality"],
                    "columns": {},
                }
                for x in event_tables.get_multiple_events(
                    c,
                    event_type_id,
                    start_ts__gte,
                    start_ts__lt,
                    comments__contains,
                    count,
                    chronological_order,
                    only_good_quality,
                )
            ]

    return {"count": len(data), "data": data}


@router.get(
    "/processed/{event_type_id}/{start_time_ms}/", response_model=ProcessedEvent
)
async def get_processed_event(
    event_type_id: int,
    start_time_ms: int,
    col: Annotated[list[str] | None, Query()] = None,
):
    path = Config.get_config().db_path

    with connection.db_connection(path) as c:
        try:
            event_data = event_tables.get_event_type_by(c, type_id=event_type_id)
            event_name = event_data["name"]
            event_class = get_event_class(event_name)
            event_class.db_connection = c
            event_obj = event_class(start_ts=start_time_ms)
            if event_obj.has_changed():
                event_obj = None
        except KeyError:
            event_obj = None

        if event_obj:
            if col:
                processed_data = {
                    col_name: getattr(event_obj, col_name)() for col_name in col
                }
            else:
                processed_data = event_obj.columns_dict()

            data = {
                "event_type_id": event_type_id,
                "name": event_name,
                "start_ts": event_obj.start_ts,
                "end_ts": event_obj.end_ts,
                "comments": event_obj.comments,
                "quality": event_obj.quality,
                "columns": processed_data,
            }

        else:
            e = event_tables.get_event(c, event_type_id, start_time_ms)
            data = {
                "event_type_id": event_type_id,
                "name": event_name,
                "start_ts": e["start_ts"],
                "end_ts": e["end_ts"],
                "comments": e["comments"],
                "quality": e["quality"],
                "columns": {},
            }

    return data
