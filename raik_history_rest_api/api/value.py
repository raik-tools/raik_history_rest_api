# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from typing import Annotated

from fastapi import APIRouter, Query

from raik_history.db import data_tables
from raik_history.db import connection

from raik_history_rest_api.api.schemas.value import ValueList
from raik_history_rest_api.conf import Config

router = APIRouter()


@router.get("/", response_model=ValueList)
async def get_latest_value_for_series(name: Annotated[list[str] | None, Query()]):
    path = Config.get_config().db_path

    with connection.db_connection(path) as c:
        data = [
            {"series_name": s, "data": data_tables.get_latest_value(c, s)} for s in name
        ]

    return {"count": len(data), "data": data}
