from pydantic import BaseModel
from raik_history.calculations.aggregates import TimeWeightedAggregateData


class TimeWeightedAggregateValue(BaseModel):
    """See RAIK_HISTORY TimeWeightedAggregateData Typed Dict"""

    series_name: str
    data: TimeWeightedAggregateData


class CalculationResultList(BaseModel):
    count: int = 0
    data: list[TimeWeightedAggregateValue] = []
