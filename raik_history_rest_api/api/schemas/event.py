from typing import Optional

from pydantic import BaseModel
from raik_history.db.event_tables import EventInterface, EventType


class EventTypes(BaseModel):
    count: int = 0
    data: list[EventType] = []


class ProcessedColumnInfo(BaseModel):
    """
    :cvar name:  The column name which corresponds to an event method name
    :cvar py_type:  The return type of the event method
    :cvar absolute_error:  A string representation of the absolute_error, for example "0.5"
    """

    name: str
    py_type: str
    absolute_error: str | None


class EventDetails(BaseModel):
    type: EventType
    processing_class: Optional[str] = None
    processed_columns: Optional[list[ProcessedColumnInfo]] = None


class EventList(BaseModel):
    count: int = 0
    data: list[EventInterface] = []


class EventUpdate(BaseModel):
    event_type_id: int
    start_ts: int
    comments: Optional[str]


class ProcessedEvent(BaseModel):
    event_type_id: int
    name: str
    start_ts: int
    end_ts: Optional[int]
    comments: Optional[str]
    quality: int
    columns: Optional[dict]


class ProcessedEventList(BaseModel):
    count: int = 0
    data: list[ProcessedEvent] = []
