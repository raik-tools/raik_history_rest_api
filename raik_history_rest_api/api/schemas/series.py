from pydantic import BaseModel
from raik_history.db.series_table import SeriesInterface


class SeriesList(BaseModel):
    count: int = 0
    data: list[SeriesInterface] = []
