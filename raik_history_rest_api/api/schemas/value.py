from pydantic import BaseModel
from raik_history.series.constants import DataTuple


class DataValue(BaseModel):
    series_name: str
    data: DataTuple


class ValueList(BaseModel):
    count: int = 0
    data: list[DataValue] = []


class SeriesValueList(BaseModel):
    count: int = 0
    series_name: str
    data: list[DataTuple] = []
