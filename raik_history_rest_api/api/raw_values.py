# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from typing import Optional

from fastapi import APIRouter
from raik_history.db import connection
from raik_history.series import series
from raik_history.series.constants import Value, Quality

from raik_history_rest_api.api.common import process_time_range
from raik_history_rest_api.api.schemas.value import SeriesValueList
from raik_history_rest_api.conf import Config

router = APIRouter()


@router.get(
    "/{name}/{start_time_ms}/{end_time_ms}/",
    response_model=SeriesValueList,
)
async def get_raw_values(
    name: str,
    start_time_ms: int,
    end_time_ms: int,
    val__is_none: Optional[bool] = None,
    val__lt: Optional[Value] = None,
    val__lte: Optional[Value] = None,
    val__eq: Optional[Value] = None,
    val__ne: Optional[Value] = None,
    val__gte: Optional[Value] = None,
    val__gt: Optional[Value] = None,
    quality__lt: Optional[Quality] = None,
    quality__lte: Optional[Quality] = None,
    quality__eq: Optional[Quality] = None,
    quality__ne: Optional[Quality] = None,
    quality__gte: Optional[Quality] = None,
    quality__gt: Optional[Quality] = None,
    include_outer_value_start: bool = False,
    include_outer_value_end: bool = False,
):
    hard_limit = 500
    start_time_ms, end_time_ms = process_time_range(start_time_ms, end_time_ms)
    path = Config.get_config().db_path

    with connection.db_connection(path) as c:
        data = list(
            series.Series.from_db(name=name, connection=c).query_data_values(
                ts__gte=start_time_ms,
                ts__lt=end_time_ms,
                val__is_none=val__is_none,
                val__lt=val__lt,
                val__lte=val__lte,
                val__eq=val__eq,
                val__ne=val__ne,
                val__gte=val__gte,
                val__gt=val__gt,
                quality__lt=quality__lt,
                quality__lte=quality__lte,
                quality__eq=quality__eq,
                quality__ne=quality__ne,
                quality__gte=quality__gte,
                quality__gt=quality__gt,
                include_outer_value_start=include_outer_value_start,
                include_outer_value_end=include_outer_value_end,
                connection=c,
                count=hard_limit,
            )
        )

    return {
        "count": len(data),
        "data": data,
        "series_name": name,
    }
