from fastapi import APIRouter


from raik_history_rest_api.api import (
    series,
    value,
    aggregate,
    calculations,
    event,
    raw_values,
)


api_router = APIRouter()
api_router.include_router(series.router, prefix="/series", tags=["series"])
api_router.include_router(value.router, prefix="/value", tags=["value"])
api_router.include_router(raw_values.router, prefix="/raw", tags=["raw"])
api_router.include_router(aggregate.router, prefix="/aggregate", tags=["aggregate"])
api_router.include_router(calculations.router, prefix="/calc", tags=["calc"])
api_router.include_router(event.router, prefix="/event", tags=["event"])

__all__ = ["api_router"]
