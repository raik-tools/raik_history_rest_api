import inspect
from typing import Optional

from raik_history.time_and_calendar import now_epoch_ms


def process_time_range(
    start_time_ms: Optional[int],
    end_time_ms: Optional[int],
) -> tuple[int, int]:
    now = now_epoch_ms()
    if start_time_ms is not None and start_time_ms < 0:
        start_time_ms = now + start_time_ms
    if end_time_ms is not None and end_time_ms < 0:
        end_time_ms = now + end_time_ms

    return start_time_ms, end_time_ms


def get_method_return_type(cls, method_name) -> type:
    """
    :raises KeyError: If method does not exist
    :raises ValueError: If is not a method
    """

    method = getattr(cls, method_name, None)
    if method is None:
        raise KeyError(f"Method {method_name} does not exist on {cls}.")
    if not callable(method):
        raise KeyError(f"Method {method_name} on {cls} is not a method.")

    # Use the inspect module to get the return type of the method
    signature = inspect.signature(method)
    return_type = signature.return_annotation
    return return_type


def get_method_return_type_string(cls, method_name) -> str:
    """
    :raises KeyError: If method does not exist
    :raises ValueError: If is not a method
    """
    return_type = get_method_return_type(cls, method_name)
    return return_type.__name__


method_annotation_absolute_error = "absolute_error"


def get_method_return_absolute_error(cls, method_name) -> str | None:
    """
    :raises KeyError: If method does not exist
    :raises ValueError: If is not a method
    """
    method = getattr(cls, method_name, None)
    if method is None:
        raise KeyError(f"Method {method_name} does not exist on {cls}.")
    if not callable(method):
        raise KeyError(f"Method {method_name} on {cls} is not a method.")

    return getattr(method, method_annotation_absolute_error, None)
