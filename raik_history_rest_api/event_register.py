"""
Used to allow api services to register event classes for
retrieving processed data.
"""
from typing import Type

from raik_history.event import Event

_event_lookup: dict[str, Type[Event]] = {}


def register_class(e: Type[Event]):
    global _event_lookup
    _event_lookup[e.NAME] = e


def get_event_class(name: str) -> Type[Event]:
    """
    :raises KeyError: If class is not registered.
    """
    global _event_lookup
    return _event_lookup[name]
