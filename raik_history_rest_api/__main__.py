import pathlib

import raik_history_rest_api.conf
from raik_history_rest_api.main import serve


def _main():
    from raik_history_rest_api.event_register import register_class

    try:
        # noinspection PyUnresolvedReferences
        from tests.db_fixture.random_event import TempEvent

        register_class(TempEvent)
    except ImportError:
        pass

    c = raik_history_rest_api.conf.Config(
        log_level="DEBUG",
        db_path=pathlib.Path(__file__).parent.parent / "var" / "db.sqlite3",
    )
    c.set_config(c)

    # serve(c, tasks=[background_task()])
    serve()


_main()
