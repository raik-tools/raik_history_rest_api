import { format } from 'date-fns'
import {navigate} from "svelte-routing";

export const VALUES_PATH = "/";
export const RAW_BASE_PATH = "/raw";
export const RAW_PATH = `${RAW_BASE_PATH}/:startTime/:endTime/`;
export const BASE_TREND_PATH = "/trend";
export const TREND_PATH = `${BASE_TREND_PATH}/:startTime/:endTime/`;
export const BASE_EVENTS_PATH = "/events";
export const EVENTS_PATH = `${BASE_EVENTS_PATH}/:startTime/:endTime/`;

export const ONE_DAY = 3600*24*1000;

// https://amiradata.com/javascript-sleep-function/
export const sleep = (seconds) => {
  return new Promise(resolve => setTimeout(resolve, seconds * 1000))
}


/**
 * Date Time handling in JS is moving.  MomentJs has a nice summary here:
 * https://momentjs.com/docs/#/-project-status/recommendations/
 *
 * Simple, thin wrappers for common date functions are provided here, in case
 * we need to switch libraries in the future.
 *
 * @param v
 */
export function msToLocaleShortDateTimeString (v: number): string {
    return format(new Date(v), "E, PPpp (O)")
}

export function formUrl(basePath: string, search?: string){
    // noinspection JSIncompatibleTypesComparison
    if (search === "" || search === undefined || search === null) return(basePath)
    else return `${basePath}?${search}`;
}

export function timeRangeSelectorUrl(pathPrefix: string, startTime: number, endTime: number, keepSearch?: boolean){
    const base = pathPrefix.replace(/\/+$/, '');
    const searchString = keepSearch === true ? (new URLSearchParams(window.location.search)).toString() : undefined;
    return formUrl(`${base}/${startTime}/${endTime}/`, searchString);
}

/**
 * Retrieve all URL search parameters given the key (allows multiple values)
 * @param key
 */
export function getUrlSearchArrayParam(key: string): string[] {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.getAll(key);
}

/**
 * Retrieve all URL search parameters that have a key that starts with....
 * @param startsWith
 */
export function getUrlSearchArrayParamStartsWith(startsWith: string): string[][] {
    const urlParams = new URLSearchParams(window.location.search);
    const retParams = [];
    for (const [key, value] of urlParams.entries() ) {
        if ( key.startsWith(startsWith) ){
            retParams.push([key, value])
        }
    }
    return retParams
}


/**
 * Update the URL Search parameters key to contain the new values.
 * @param key
 * @param values
 */
export function updateSearchParams(key: string, values: string[] | null) {
    // Get the current search parameters
    const initUrlParams = new URLSearchParams(window.location.search);

    // Create a new Search object
    const params = [];
    const replaceParams = [];
    for (const [k, v] of initUrlParams.entries() ) {
        if ( k === key ) {
            replaceParams.push([k, v]);
        }
        else {
            params.push([k, v]);
        }
    }

    // Update the search object
    if ( values !== null ){
        values.forEach((v) => params.push([key, v]));
    }

    // Create new Parameters
    const searchString = new URLSearchParams(params).toString();
    const newUrl = formUrl(window.location.pathname, searchString);
    navigate(newUrl, { replace: true });
}


export function roundWithAbsoluteError(value: Number, absoluteError: string): String{
    // Calculate the maximum allowed deviation
    const maxDeviation = Number(absoluteError) / 2.0;

     // Round the value to the nearest integer within the error margin
      const roundedValue = Math.round(value / maxDeviation) * maxDeviation;

      // Determine the number of decimal places based on the absoluteError
      const decimalPlaces = Math.max(-Math.floor(Math.log10(Number(absoluteError))), 0);

      // Use toFixed with the calculated decimalPlaces
      return roundedValue.toFixed(decimalPlaces);
}