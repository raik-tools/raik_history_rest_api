// @ts-ignore
import App from './App.svelte';
import '../style_loader.scss';

console.log("Loading RAIK History Web");

const app = new App({
	target: document.getElementById("app"),
	props: {}
});

// noinspection JSUnusedGlobalSymbols
export default app;


