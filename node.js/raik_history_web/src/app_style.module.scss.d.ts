export interface AppStyles {
  "scheme-main": string;
  background: string;
  "background-darker": string;
  primary: string;
  info: string;
  success: string;
  warning: string;
  danger: string;
  error: string;
  transparent: string;
}

export const styles: AppStyles;

export default styles;
