/* tslint:disable */
/* eslint-disable */
/**
 * FastAPI
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as runtime from '../runtime';
import type {
  HTTPValidationError,
  SeriesValueList,
} from '../models/index';
import {
    HTTPValidationErrorFromJSON,
    HTTPValidationErrorToJSON,
    SeriesValueListFromJSON,
    SeriesValueListToJSON,
} from '../models/index';

export interface GetRawValuesApiRawNameStartTimeMsEndTimeMsGetRequest {
    name: any;
    start_time_ms: any;
    end_time_ms: any;
    val__is_none?: any;
    val__lt?: any | null;
    val__lte?: any | null;
    val__eq?: any | null;
    val__ne?: any | null;
    val__gte?: any | null;
    val__gt?: any | null;
    quality__lt?: any;
    quality__lte?: any;
    quality__eq?: any;
    quality__ne?: any;
    quality__gte?: any;
    quality__gt?: any;
    include_outer_value_start?: any;
    include_outer_value_end?: any;
}

/**
 * 
 */
export class RawApi extends runtime.BaseAPI {

    /**
     * Get Raw Values
     */
    async getRawValuesApiRawNameStartTimeMsEndTimeMsGetRaw(requestParameters: GetRawValuesApiRawNameStartTimeMsEndTimeMsGetRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<SeriesValueList>> {
        if (requestParameters.name === null || requestParameters.name === undefined) {
            throw new runtime.RequiredError('name','Required parameter requestParameters.name was null or undefined when calling getRawValuesApiRawNameStartTimeMsEndTimeMsGet.');
        }

        if (requestParameters.start_time_ms === null || requestParameters.start_time_ms === undefined) {
            throw new runtime.RequiredError('start_time_ms','Required parameter requestParameters.start_time_ms was null or undefined when calling getRawValuesApiRawNameStartTimeMsEndTimeMsGet.');
        }

        if (requestParameters.end_time_ms === null || requestParameters.end_time_ms === undefined) {
            throw new runtime.RequiredError('end_time_ms','Required parameter requestParameters.end_time_ms was null or undefined when calling getRawValuesApiRawNameStartTimeMsEndTimeMsGet.');
        }

        const queryParameters: any = {};

        if (requestParameters.val__is_none !== undefined) {
            queryParameters['val__is_none'] = requestParameters.val__is_none;
        }

        if (requestParameters.val__lt !== undefined) {
            queryParameters['val__lt'] = requestParameters.val__lt;
        }

        if (requestParameters.val__lte !== undefined) {
            queryParameters['val__lte'] = requestParameters.val__lte;
        }

        if (requestParameters.val__eq !== undefined) {
            queryParameters['val__eq'] = requestParameters.val__eq;
        }

        if (requestParameters.val__ne !== undefined) {
            queryParameters['val__ne'] = requestParameters.val__ne;
        }

        if (requestParameters.val__gte !== undefined) {
            queryParameters['val__gte'] = requestParameters.val__gte;
        }

        if (requestParameters.val__gt !== undefined) {
            queryParameters['val__gt'] = requestParameters.val__gt;
        }

        if (requestParameters.quality__lt !== undefined) {
            queryParameters['quality__lt'] = requestParameters.quality__lt;
        }

        if (requestParameters.quality__lte !== undefined) {
            queryParameters['quality__lte'] = requestParameters.quality__lte;
        }

        if (requestParameters.quality__eq !== undefined) {
            queryParameters['quality__eq'] = requestParameters.quality__eq;
        }

        if (requestParameters.quality__ne !== undefined) {
            queryParameters['quality__ne'] = requestParameters.quality__ne;
        }

        if (requestParameters.quality__gte !== undefined) {
            queryParameters['quality__gte'] = requestParameters.quality__gte;
        }

        if (requestParameters.quality__gt !== undefined) {
            queryParameters['quality__gt'] = requestParameters.quality__gt;
        }

        if (requestParameters.include_outer_value_start !== undefined) {
            queryParameters['include_outer_value_start'] = requestParameters.include_outer_value_start;
        }

        if (requestParameters.include_outer_value_end !== undefined) {
            queryParameters['include_outer_value_end'] = requestParameters.include_outer_value_end;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/api/raw/{name}/{start_time_ms}/{end_time_ms}/`.replace(`{${"name"}}`, encodeURIComponent(String(requestParameters.name))).replace(`{${"start_time_ms"}}`, encodeURIComponent(String(requestParameters.start_time_ms))).replace(`{${"end_time_ms"}}`, encodeURIComponent(String(requestParameters.end_time_ms))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => SeriesValueListFromJSON(jsonValue));
    }

    /**
     * Get Raw Values
     */
    async getRawValuesApiRawNameStartTimeMsEndTimeMsGet(requestParameters: GetRawValuesApiRawNameStartTimeMsEndTimeMsGetRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<SeriesValueList> {
        const response = await this.getRawValuesApiRawNameStartTimeMsEndTimeMsGetRaw(requestParameters, initOverrides);
        return await response.value();
    }

}
