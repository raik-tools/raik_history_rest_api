/* tslint:disable */
/* eslint-disable */
/**
 * FastAPI
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface EventTypes
 */
export interface EventTypes {
    /**
     * 
     * @type {any}
     * @memberof EventTypes
     */
    count?: any | null;
    /**
     * 
     * @type {any}
     * @memberof EventTypes
     */
    data?: any | null;
}

/**
 * Check if a given object implements the EventTypes interface.
 */
export function instanceOfEventTypes(value: object): boolean {
    let isInstance = true;

    return isInstance;
}

export function EventTypesFromJSON(json: any): EventTypes {
    return EventTypesFromJSONTyped(json, false);
}

export function EventTypesFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventTypes {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'count': !exists(json, 'count') ? undefined : json['count'],
        'data': !exists(json, 'data') ? undefined : json['data'],
    };
}

export function EventTypesToJSON(value?: EventTypes | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'count': value.count,
        'data': value.data,
    };
}

