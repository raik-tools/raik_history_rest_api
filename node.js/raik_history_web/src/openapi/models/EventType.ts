/* tslint:disable */
/* eslint-disable */
/**
 * FastAPI
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface EventType
 */
export interface EventType {
    /**
     * 
     * @type {any}
     * @memberof EventType
     */
    event_type_id: any | null;
    /**
     * 
     * @type {any}
     * @memberof EventType
     */
    name: any | null;
    /**
     * 
     * @type {any}
     * @memberof EventType
     */
    description: any | null;
    /**
     * 
     * @type {any}
     * @memberof EventType
     */
    time_type: any | null;
    /**
     * 
     * @type {any}
     * @memberof EventType
     */
    time_stamp_type: any | null;
}

/**
 * Check if a given object implements the EventType interface.
 */
export function instanceOfEventType(value: object): boolean {
    let isInstance = true;
    isInstance = isInstance && "event_type_id" in value;
    isInstance = isInstance && "name" in value;
    isInstance = isInstance && "description" in value;
    isInstance = isInstance && "time_type" in value;
    isInstance = isInstance && "time_stamp_type" in value;

    return isInstance;
}

export function EventTypeFromJSON(json: any): EventType {
    return EventTypeFromJSONTyped(json, false);
}

export function EventTypeFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventType {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'event_type_id': json['event_type_id'],
        'name': json['name'],
        'description': json['description'],
        'time_type': json['time_type'],
        'time_stamp_type': json['time_stamp_type'],
    };
}

export function EventTypeToJSON(value?: EventType | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'event_type_id': value.event_type_id,
        'name': value.name,
        'description': value.description,
        'time_type': value.time_type,
        'time_stamp_type': value.time_stamp_type,
    };
}

