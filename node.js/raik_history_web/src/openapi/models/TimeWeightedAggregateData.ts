/* tslint:disable */
/* eslint-disable */
/**
 * FastAPI
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface TimeWeightedAggregateData
 */
export interface TimeWeightedAggregateData {
    /**
     * 
     * @type {any}
     * @memberof TimeWeightedAggregateData
     */
    result: any | null;
    /**
     * 
     * @type {any}
     * @memberof TimeWeightedAggregateData
     */
    units: any | null;
    /**
     * 
     * @type {any}
     * @memberof TimeWeightedAggregateData
     */
    included_points: any | null;
    /**
     * 
     * @type {any}
     * @memberof TimeWeightedAggregateData
     */
    excluded_points: any | null;
    /**
     * 
     * @type {any}
     * @memberof TimeWeightedAggregateData
     */
    included_seconds: any | null;
    /**
     * 
     * @type {any}
     * @memberof TimeWeightedAggregateData
     */
    excluded_seconds: any | null;
    /**
     * 
     * @type {any}
     * @memberof TimeWeightedAggregateData
     */
    start_time: any | null;
    /**
     * 
     * @type {any}
     * @memberof TimeWeightedAggregateData
     */
    end_time: any | null;
}

/**
 * Check if a given object implements the TimeWeightedAggregateData interface.
 */
export function instanceOfTimeWeightedAggregateData(value: object): boolean {
    let isInstance = true;
    isInstance = isInstance && "result" in value;
    isInstance = isInstance && "units" in value;
    isInstance = isInstance && "included_points" in value;
    isInstance = isInstance && "excluded_points" in value;
    isInstance = isInstance && "included_seconds" in value;
    isInstance = isInstance && "excluded_seconds" in value;
    isInstance = isInstance && "start_time" in value;
    isInstance = isInstance && "end_time" in value;

    return isInstance;
}

export function TimeWeightedAggregateDataFromJSON(json: any): TimeWeightedAggregateData {
    return TimeWeightedAggregateDataFromJSONTyped(json, false);
}

export function TimeWeightedAggregateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): TimeWeightedAggregateData {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'result': json['result'],
        'units': json['units'],
        'included_points': json['included_points'],
        'excluded_points': json['excluded_points'],
        'included_seconds': json['included_seconds'],
        'excluded_seconds': json['excluded_seconds'],
        'start_time': json['start_time'],
        'end_time': json['end_time'],
    };
}

export function TimeWeightedAggregateDataToJSON(value?: TimeWeightedAggregateData | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'result': value.result,
        'units': value.units,
        'included_points': value.included_points,
        'excluded_points': value.excluded_points,
        'included_seconds': value.included_seconds,
        'excluded_seconds': value.excluded_seconds,
        'start_time': value.start_time,
        'end_time': value.end_time,
    };
}

