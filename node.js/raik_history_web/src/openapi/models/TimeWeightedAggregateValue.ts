/* tslint:disable */
/* eslint-disable */
/**
 * FastAPI
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import type { TimeWeightedAggregateData } from './TimeWeightedAggregateData';
import {
    TimeWeightedAggregateDataFromJSON,
    TimeWeightedAggregateDataFromJSONTyped,
    TimeWeightedAggregateDataToJSON,
} from './TimeWeightedAggregateData';

/**
 * See RAIK_HISTORY TimeWeightedAggregateData Typed Dict
 * @export
 * @interface TimeWeightedAggregateValue
 */
export interface TimeWeightedAggregateValue {
    /**
     * 
     * @type {any}
     * @memberof TimeWeightedAggregateValue
     */
    series_name: any | null;
    /**
     * 
     * @type {TimeWeightedAggregateData}
     * @memberof TimeWeightedAggregateValue
     */
    data: TimeWeightedAggregateData;
}

/**
 * Check if a given object implements the TimeWeightedAggregateValue interface.
 */
export function instanceOfTimeWeightedAggregateValue(value: object): boolean {
    let isInstance = true;
    isInstance = isInstance && "series_name" in value;
    isInstance = isInstance && "data" in value;

    return isInstance;
}

export function TimeWeightedAggregateValueFromJSON(json: any): TimeWeightedAggregateValue {
    return TimeWeightedAggregateValueFromJSONTyped(json, false);
}

export function TimeWeightedAggregateValueFromJSONTyped(json: any, ignoreDiscriminator: boolean): TimeWeightedAggregateValue {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'series_name': json['series_name'],
        'data': TimeWeightedAggregateDataFromJSON(json['data']),
    };
}

export function TimeWeightedAggregateValueToJSON(value?: TimeWeightedAggregateValue | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'series_name': value.series_name,
        'data': TimeWeightedAggregateDataToJSON(value.data),
    };
}

