import {defineConfig} from 'vite'
import {svelte} from '@sveltejs/vite-plugin-svelte'

const root_name = "raik_history_web.bundle";
const out_root = "../../raik_history_rest_api/static/raik_history_web_bundle/"


// https://vitejs.dev/config/
// noinspection JSUnusedGlobalSymbols
export default defineConfig({
    plugins: [
        svelte()
    ],
    build: {
        outDir: out_root,
        lib: {
            entry: "./src/data_view/main.ts",
            name: "raik_history_web",
            formats: ["iife"],
            fileName: root_name
        },
        sourcemap: true,
    }
})
