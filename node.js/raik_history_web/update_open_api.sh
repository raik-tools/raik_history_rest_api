#!/bin/bash

npm run openapi-generator-version-7
npm run generate-openapi

echo "Patching OpenAPI output due to incomplete implementation of the generator."

EVENT_API_FILE=`find -name EventApi.ts`

SEARCH_EVENT_TEXT="queryParameters\['f'\] = requestParameters\.f;"
NEW_EVENT_TEXT="Object.entries(requestParameters.f).forEach(([k, v]) => queryParameters[k]=v);"
echo "Updating ${EVENT_API_FILE}"
sed -i -e "s|$SEARCH_EVENT_TEXT|$NEW_EVENT_TEXT|g" $EVENT_API_FILE