import datetime
import os
import pathlib
import random
from typing import Type

from raik_history.quality import QualityCodes
from raik_history.series.constants import (
    TIME_STAMP_TYPE_LOCAL,
    DATA_TYPE_STRING,
    DATA_TYPE_INT,
    DATA_TYPE_REAL,
    DATA_TYPE_BOOL,
)
from raik_history.series.series import Series
from raik_history.series.series_set import SeriesSet
from raik_history.time_and_calendar import create_time_aware, datetime_to_epoch_ms

VAR_DIR = pathlib.Path(__file__).parent.parent.parent / "var"


class TempSeries(SeriesSet):

    TIME_STAMP_TYPE = TIME_STAMP_TYPE_LOCAL

    str_data = Series("data.str", DATA_TYPE_STRING)
    bool_data = Series("data.bool_data", DATA_TYPE_BOOL)
    real_data = Series("data.real_data", DATA_TYPE_REAL)
    int_data = Series("data.int_data", DATA_TYPE_INT)

    SERIES = (
        str_data,
        bool_data,
        real_data,
        int_data,
    )


# noinspection DuplicatedCode
def create_test_db(tmp_path=VAR_DIR / "db.sqlite3") -> Type[TempSeries]:
    string_options = "The cat sat on the mat wearing a hat".split(" ")
    series_start = create_time_aware(2023, 6, 1, 6, zone_string="Australia/Brisbane")

    time_stamps = [
        series_start + datetime.timedelta(seconds=x * 10) for x in range(100)
    ]

    if tmp_path.exists():
        os.remove(tmp_path)

    class TestSeriesClass(TempSeries):
        db_path = tmp_path

    TestSeriesClass.create_series()

    for ts in time_stamps:
        ts = datetime_to_epoch_ms(ts)
        str_val = random.choice(string_options)
        bool_val = random.random() > 0.5
        real_val = 100 * random.random()
        int_val = int(100 * random.random())

        TestSeriesClass.add_values_to_db(
            values={
                TestSeriesClass.str_data.name: str_val,
                TestSeriesClass.bool_data.name: bool_val,
                TestSeriesClass.real_data.name: real_val,
                TestSeriesClass.int_data.name: int_val,
            },
            time_stamp=ts,
            quality=QualityCodes.GOOD_NON_SPECIFIC,
        )

    return TestSeriesClass


if __name__ == "__main__":
    create_test_db()
