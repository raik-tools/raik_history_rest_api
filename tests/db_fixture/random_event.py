import datetime
import functools
import os
import pathlib
import random
from typing import Type

from raik_history.event import Event
from raik_history.quality import QualityCodes
from raik_history.series.constants import (
    TIME_STAMP_TYPE_LOCAL,
    DATA_TYPE_STRING,
    DATA_TYPE_INT,
    DATA_TYPE_REAL,
    DATA_TYPE_BOOL,
    DataTuple,
)
from raik_history.series.series import Series
from raik_history.series.series_set import SeriesSet
from raik_history.time_and_calendar import create_time_aware, datetime_to_epoch_ms
from raik_history.units_of_measure import specify_absolute_error

VAR_DIR = pathlib.Path(__file__).parent.parent.parent / "var"


class TempSeriesForEvent(SeriesSet):

    TIME_STAMP_TYPE = TIME_STAMP_TYPE_LOCAL

    str_data = Series("event.str_data", DATA_TYPE_STRING)
    bool_data = Series("event.bool_data", DATA_TYPE_BOOL)
    real_data = Series("event.real_data", DATA_TYPE_REAL)
    int_data = Series("event.int_data", DATA_TYPE_INT)

    SERIES = (
        str_data,
        bool_data,
        real_data,
        int_data,
    )


class TempEvent(Event):
    NAME = "temp_event"
    DESCRIPTION = "An event class used for testing"

    trigger_series = TempSeriesForEvent.bool_data
    interpolate_at_start_boundary = True
    interpolate_at_end_boundary = True

    columns = ["duration", "int_total", "float_ish_value"]

    @functools.cache
    def int_total(self) -> int:
        data = self.get_data()

        total = int(data[self.RelatedSeries.int_data.name].sum())

        return total

    @functools.cache
    @specify_absolute_error("0.05")
    def float_ish_value(self) -> float:
        data = self.get_data()
        total = int(data[self.RelatedSeries.int_data.name].sum())
        total *= random.random()
        return total

    class RelatedSeries(SeriesSet):
        """
        References are made to related series for data retrieval related to this event.
        """

        real_data = TempSeriesForEvent.real_data
        int_data = TempSeriesForEvent.int_data

        SERIES = (
            TempSeriesForEvent.real_data,
            TempSeriesForEvent.int_data,
        )

    @classmethod
    def event_open_detected(cls, last_val: DataTuple, val: DataTuple) -> bool:
        if val[1] is None or last_val[1] is None:
            return False
        return not last_val[1] and val[1]

    @classmethod
    def event_close_detected(cls, last_val: DataTuple, val: DataTuple) -> bool:
        if val[1] is None or last_val[1] is None:
            return False
        return last_val[1] and not val[1]


# noinspection DuplicatedCode
def create_test_db(
    tmp_path=VAR_DIR / "db.sqlite3", db_connection=None
) -> Type[TempSeriesForEvent]:
    """
    The test DB is gaurunteed to contain the following:

    * A temp_event entry (id 1)
    * Data for related series randomly generated from 2023-6-1 6:00 AM AEST
    * Data is updated every 10 seconds but compressio might remove values
    * Events randomly created each time event.bool_data changes state
    * About half of the events will have no comment, the other half will be a random word from
      "The cat sat on the mat wearing a hat"

    """
    string_options = "The cat sat on the mat wearing a hat".split(" ")
    series_start = create_time_aware(2023, 6, 1, 6, zone_string="Australia/Brisbane")

    time_stamps = [
        series_start + datetime.timedelta(seconds=x * 10) for x in range(100)
    ]

    if db_connection:
        test_path = None
        test_connection = db_connection
    else:
        if tmp_path.exists():
            os.remove(tmp_path)
        test_path = tmp_path
        test_connection = None

    class TestSeriesClass(TempSeriesForEvent):
        db_path = test_path
        db_connection = test_connection

    class TestEventClass(TempEvent):
        db_path = test_path
        db_connection = test_connection

    TestSeriesClass.create_series()

    for ts in time_stamps:
        ts = datetime_to_epoch_ms(ts)
        str_val = random.choice(string_options)
        bool_val = random.random() > 0.5
        real_val = 100 * random.random()
        int_val = int(100 * random.random())

        TestSeriesClass.add_values_to_db(
            values={
                TestSeriesClass.str_data.name: str_val,
                TestSeriesClass.bool_data.name: bool_val,
                TestSeriesClass.real_data.name: real_val,
                TestSeriesClass.int_data.name: int_val,
            },
            time_stamp=ts,
            quality=QualityCodes.GOOD_NON_SPECIFIC,
        )

    TestEventClass.create_or_close_events()
    TestEventClass.rescan_event_qualities()

    # noinspection PyTypeChecker
    comment_options: list[str | None] = [None for _ in string_options] + string_options
    for e in TestEventClass.get_events():
        e.comments = random.choice(comment_options)
        e.save()

    return TestSeriesClass


if __name__ == "__main__":
    create_test_db()
