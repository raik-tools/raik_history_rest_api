"""
This module focuses on extensions of the event component through inheritance.

Note, FastAPI has a test client here:  https://fastapi.tiangolo.com/tutorial/testing/

However, this module is not using it as the intent is not to perform interface tests
but to test functions within the interface.
"""
from typing import Type
from unittest.mock import Mock

import pytest

from raik_history_rest_api import event_register
from raik_history_rest_api.api.event import get_processed_events
from raik_history_rest_api.conf import Config
from tests.db_fixture.random_event import create_test_db, TempEvent


@pytest.fixture
def tmp_db_fixture(tmp_db):
    """
    See docstring for create_test_db
    """
    create_test_db(db_connection=tmp_db)
    return TempEvent


@pytest.fixture
def api_config(tmp_db_path):
    """
    This fixture forcibly updates the Config object with the intention
    of returning it back to the original state after a test.
    """

    # See docstring
    # noinspection PyProtectedMember
    original_config = Config._current_config

    new_config = Config(db_path=tmp_db_path)
    # See docstring
    # noinspection PyProtectedMember
    Config._current_config = new_config
    yield
    # See docstring
    # noinspection PyProtectedMember
    Config._current_config = original_config


@pytest.fixture
def registered_event_class(tmp_db_path) -> Type[TempEvent]:
    class RegisteredEventClass(TempEvent):
        pass

    # noinspection PyProtectedMember
    event_register._event_lookup[RegisteredEventClass.NAME] = RegisteredEventClass

    yield RegisteredEventClass

    # noinspection PyProtectedMember
    del event_register._event_lookup[RegisteredEventClass.NAME]


@pytest.mark.asyncio
async def test_get_processed_events__comments_contain__expect_matching_return_events(
    tmp_db_fixture, api_config, registered_event_class
):
    request_mock = Mock()
    request_mock.query_params = {}
    data = await get_processed_events(1, request_mock, comments__contains="at")
    for e in data["data"]:
        assert "at" in e["comments"]


@pytest.mark.asyncio
async def test_get_processed_events__filter__expect_matching_return_events(
    tmp_db_fixture, api_config, registered_event_class
):
    request_mock = Mock()
    request_mock.query_params = {"int_total__gte": 100}
    data = await get_processed_events(1, request_mock, col=["int_total"])
    for e in data["data"]:
        assert e["columns"]["int_total"] >= 100
