import uuid
from typing import Type

import pytest
from raik_history.db.connection import db_connection

from tests.db_fixture.random_series import TempSeries, create_test_db


@pytest.fixture
def temp_series_set(tmp_path) -> Type[TempSeries]:
    return create_test_db(tmp_path)


@pytest.fixture
def tmp_db_path(tmp_path):
    return tmp_path / f"{uuid.uuid4()}.db"


@pytest.fixture
def tmp_db(tmp_db_path):
    with db_connection(tmp_db_path) as c:
        yield c


@pytest.fixture(autouse=True)
def clear_caches():
    from raik_history import reset_caches

    reset_caches()
