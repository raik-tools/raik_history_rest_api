=====================
RAIK History REST API
=====================

Introduction
============

This module is intended as an quick and easy drop in for interacting
with a raik_history database via a REST API

Limitations
===========

This is still very much in its early stages.


Documentation
=============

Roadmap
-------

The first version of this project is just enough to fetch near-time data
from a raik_history database.

[x] Create a REST API that returns a list of series in a raik_history database
[x] Create a REST API that returns a recent value for a single series
[x] Create a REST API that returns a recent value for a set of series
[x] Create a REST API for querying aggregate values over a time range
[x]  Update series configuration of raik_history to include a default rate reporting

GOAL:  To be able to explore data in a DB live including events, event triggers, and event data

[x] Migrate to FastAPI from Falcon (to be consistent with raik_dashboard)
[x] Test OpenAPI Docs
[x] Create an interface for events
[x] Consider adding a rudimentary data explorer / trend viewer.  I started creating
    raik_history_web, but this has gone nowhere and it might just be easier to create a demo
    application within this app.

Web View Progress

[x] Establish basic routing with state in qs so that a url can be used to produce a display
[x] Create a navigation mock layout in DataDisplay
[x] Add a series current value query
[x] Add a simple trend view
[x] Add an event view

[x] Add an event comment edit feature to the web view
[x] Create an API to GET/POST individual events and test posting a comment.
[x] Update OpenAPI interface
[x] Update the event table to include an edit field for comments.  As this is not a typical usage
    it will work with an edit enable and save for an individual row (cell) only.
[x] Before updating versions, consider some shortcuts like "last 24h" or something in the URL.  Maybe just allow negative in the path?

[...] Add additional filters to events to make finding key events easier
[...] For tesla logger, filtering by comments (search terms) or calculated columns (min range) will be useful
    Something coarse like a filter control at the top of a table that takes symbols like < > = for numbers
    or text for other.
[x] Enable filtering in REST API
[x] Test comments filter
[x] Test processed filter on debug event classes
[x] Should the processed filter have some kind of type declaration convention, perhaps in the variable name?
    since there is no type declaration mechanism (unless I introspect the event_class column name functions)
    Should the filter have a __gt etc...  filter.  Having some reusable defaults might make it easier to
    implement.  This would need to be updateed on Event in raik_history.
[x] Update OpenAPI
[x] Update query in Events.svelte for data in updateData

Goal:  General Updates

[n] Should Search Params be a store or can they move into the components as a local variable?
    reactivity seems to be quite nuanced in Svelte and perhaps a store is clearer?
[ ] Update trend so that series is a multi select.  This requires an updated API on the server.
[ ] Create a REST endpoint to retrieve quality codes or convert these to strings in value
[ ] Warning:  This needs to be reviewed against REST standards
[ ] WARNING:  This is an async application but sqlite calls are synchronous... RAIK_HISTORY needs to
    move to asyncio before this can.  sqlalchemy has a core (non ORM) mode that will probably fit
    the spot well.
[ ] Fix REST API endpoints by cleaning up querystring and args in path (see calculations)
[ ] Find a better datetime picker and stop page scrolling on each click (if possible).
[ ] Add more rawValues query options


[x] Is CSV as param less correct than CSV as path?  If param order does not matter, can multiple
    params be provided  ?name=...&name===?  Although csv is more condensed.  The ideal way is
    to create a resource...  I will find some web guides on this and add it to docs.
https://fastapi.tiangolo.com/tutorial/query-params-str-validations/#query-parameter-list-multiple-values


**** Other Apps Updates ****

[ ] RAIK History update_event should allow for optional parameters and not require all parameters
    to be specified, otherwise technical debt will occur (see post_raw_event needing to specify all data).
[x] I am in the process of migrating falcon classes to FastAPI api router functions
[ ] The API has changed slightly, especially paths and return types.  Dashboard will need
    fixing.

Code generation
===============

Following this guide:  https://www.stefanwille.com/2021/05/2021-05-30-openapi-code-generator-for-typescript

API Endpoints
=============

What are the types of resources that may be desired from this interface?

* A list of series objects
* Values as a snapshot in time
* Aggregates
* Calculations
* Events and related event data

refer to http://localhost:8080/docs for more information on API endpoints.

License
=======

GPL-3.0-or-later
